### IPL Scorecard

    Enjoy your favourite IPL team's score on your terminal.

### Instructions To Use

1. Install urllib, BeautifulSoup and PrettyTable using pip.
2. Run the following command to see the score of your favourite team.

```
    python app.py rcb srh
```
