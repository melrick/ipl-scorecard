from bs4 import BeautifulSoup
import urllib
import re
from prettytable import PrettyTable
import sys

source = urllib.urlopen('http://www.espncricinfo.com/ci/engine/match/index.html?view=live').read()
soup = BeautifulSoup(source,'lxml')

table = PrettyTable()
table.field_names = ["Team","Score","Overs"]

def teamName(x):
    teams={'rcb':'Royal Challengers Bangalore',
            'mi':'Mumbai Indians',
            'srh':'Sunrisers Hyderabad',
            'kkr':'Kolkata Knight Riders',
            'rps':'Rising Pune Supergiant',
            'kxip':'Kings XI Punjab',
            'dd':'Delhi Daredevils',
            'gl':'Gujarat Lions'
    }
    try:
        return teams[x]
    except KeyError:
        return 0

team1 = teamName(sys.argv[1])
team2 = teamName(sys.argv[2])


score1 = overs1 = score2 = overs2 = ""

divs1 = soup.find_all("div",{"class":"innings-info-1"})
for div in divs1:
    if re.search(team1,div.text):
        data = re.sub(r'\s+',"_",div.text)
        data = re.split('_',data)
        if data[-4] is not None:
            score1 = data[-4]
        if data[-3] is not None:
            overs1 = data[-3]
            overs1 = re.sub(r'[\(\)]'," ",overs1)
        table.add_row([team1,score1,overs1])

divs2 = soup.find_all("div",{"class":"innings-info-2"})
for div in divs2:
    if re.search(team2,div.text):
        data = re.sub(r'\s+',"_",div.text)
        data = re.split('_',data)
        if len(data) > 5:
            if data[-4] is not None:
                score2 = data[-4]
            if data[-3] is not None:
                overs2 = data[-3]
                overs2 = re.sub(r'[\(\)]'," ",overs2)
            table.add_row([team2,score2,overs2])


print table
